<a name="v0.0.0.3"></a>
## [v0.0.0.3](https://gitlab.com/RONBAR/test-project/compare/v0.0.0.2...v0.0.0.3)


<a name="v0.0.0.2"></a>
## [v0.0.0.2](https://gitlab.com/RONBAR/test-project/compare/v0.0.0.1...v0.0.0.2) (2019-11-21)

### Docs
- fixed up the docs a bit ([ace2e8e](https://gitlab.com/RONBAR/test-project/commit/ace2e8e516b3c7728da62e8deeba054e5757fddb))


<a name="v0.0.0.1"></a>
## [v0.0.0.1](https://gitlab.com/RONBAR/test-project/compare/3c38112e4eb8a276522b821c34d98c1b6650ade7...v0.0.0.1) (2019-11-21)

### Bug Fixes
- **core:** update dependency figures to v3.1.0 (#468) ([3acc938](https://gitlab.com/RONBAR/test-project/commit/3acc93861c131274cb36211531eb36a5bdad5cb1)), related to [#6](https://gitlab.com/RONBAR/test-project/issues/2)

### Chores
- **configuration:** do some refactoring (#1) ([ab3a547](https://gitlab.com/RONBAR/test-project/commit/ab3a547e9f5bdab640ebcc8af0e1beb89c15660a)), related to [#6](https://gitlab.com/RONBAR/test-project/issues/2)
- **release:** v0.0.0.1 ([df00a98](https://gitlab.com/RONBAR/test-project/commit/df00a9837d51d3a6cc7114c34bd2ffe82cbd35d6))

### Docs
- **README:** update readme (#2) ([096d835](https://gitlab.com/RONBAR/test-project/commit/096d83587512883c623a0e9f2fef05fb4fbab81a)), related to [#6](https://gitlab.com/RONBAR/test-project/issues/2)
- **README:** update readme (#2) ([4be2cb9](https://gitlab.com/RONBAR/test-project/commit/4be2cb953de37bf564dae09e3308993c29d67221)), related to [#6](https://gitlab.com/RONBAR/test-project/issues/2)

### New Features
- **base:** add new base class (#7) ([79f7f3e](https://gitlab.com/RONBAR/test-project/commit/79f7f3e77e4916f156e976676bd6b67bf506465a)), related to [#6](https://gitlab.com/RONBAR/test-project/issues/2)

### Tests
- **base-test:** add new base class test (#8) ([b6dcda6](https://gitlab.com/RONBAR/test-project/commit/b6dcda6e0034af2c1f9c27ba7543767e7121ae67)), related to [#6](https://gitlab.com/RONBAR/test-project/issues/2)


